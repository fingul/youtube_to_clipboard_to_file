import os
import time
import logging
import datetime

import xerox


logging.basicConfig(level=logging.INFO)

TIME_SLEEP = 0.1

session_name = datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")

path = "~/youdown/{session_name}.txt".format(session_name=session_name)
path = os.path.expanduser(path)
dir_name = os.path.dirname(path)

if not os.path.exists(dir_name):
    os.makedirs(dir_name)

logging.info("save file to : {}".format(path))

c_before = None

try:
    with open(path, "wb") as f:
        while 1:
            c = xerox.paste()
            if c_before != c and c.lower().startswith("http"):
                logging.info("write URL : {}".format(c))
                f.write(c + "\n")
                c_before = c
            time.sleep(TIME_SLEEP)
except KeyboardInterrupt:
    pass

os.system("open {}".format(dir_name))

cmd = "youtube-dl --batch-file {path} --output " \
      "'~/youdown/{session_name}/%(autonumber)s_%(title)s-%(id)s.%(ext)s'".format(path=path, session_name=session_name)

print cmd

#os.system("open {}".format(os.path.dirname(path)))



